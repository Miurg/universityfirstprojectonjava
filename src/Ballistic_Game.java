import com.sun.javafx.scene.paint.GradientUtils;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.PathTransition;
import javafx.animation.PathTransition.OrientationType;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.scene.shape.Rectangle;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Duration;
import javafx.event.ActionEvent;

import java.awt.*;
import java.util.ArrayList;

import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class Ballistic_Game extends Application{
    Rectangle player1 = new Rectangle();
    Rectangle player2 = new Rectangle();
    Rectangle blockbullet = new Rectangle();

    int widthwindow = 1000;
    int heightwindow = 800;

    PathTransition pathTransition = new PathTransition();
    EventHandler<ActionEvent> playbutton;

    TextField angleTextField=new TextField();;
    TextField speedTextField=new TextField();;
    TextField playerpositionTextField = new TextField();;

    Slider angleSlider = new Slider(90,179,1);
    Slider speedSlider = new Slider(1,10,1);
    Slider playerpositionSlider = new Slider(1,500,1);

    Label angleLabel = new Label("Угол");
    Label speedLabel = new Label("Скорость");
    Label playerpositionLabel = new Label("Позиция игрока");
    Label whosemoveLabel = new Label("Ход красного игрока!");

    int whosemove;
    int win = 0;
    boolean animationisnow = false;;

    double angleRed=91.0;
    double speedRed=1.0;
    double angleBlue=91.0;
    double speedBlue=1.0;

    Timeline timeline1;
    @Override
    public void start(Stage primaryStage) {
        Group root = new Group();
        whosemove = 1;
        win = 0;
        playbuttonmethod2(primaryStage,root);
        initial();

        Button btnshoot=new Button("Выстрел");
        btnshoot.setOnAction(playbutton);
        btnshoot.setTranslateY(90);

        root.getChildren().addAll(btnshoot,
                speedTextField, angleTextField, playerpositionTextField,
                angleLabel,speedLabel,playerpositionLabel,
                angleSlider,speedSlider,playerpositionSlider,
                player1,player2,
                blockbullet,
                whosemoveLabel);
        Scene scene = new Scene(root,widthwindow,heightwindow,Color.WHEAT);
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.setTitle("Баллистическая игра");
        primaryStage.centerOnScreen();
        primaryStage.show();
        scene.getWidth();
    }

    private void initial()
    {
        angleSlider.setOnMouseDragged(arg0 -> angleTextField.setText(Double.toString((int)angleSlider.getValue() - 90)));
        angleSlider.setOnMouseClicked(arg0 -> angleTextField.setText(Double.toString((int)angleSlider.getValue() - 90)));
        speedSlider.setOnMouseDragged(arg0 -> speedTextField.setText(Double.toString((int)speedSlider.getValue())));
        speedSlider.setOnMouseClicked(arg0 -> speedTextField.setText(Double.toString((int)speedSlider.getValue())));
        playerpositionSlider.setOnMouseDragged(arg0 ->
        {
            if (pathTransition.getStatus() != Animation.Status.RUNNING) {
                playerpositionTextField.setText(Double.toString((int) playerpositionSlider.getValue()));
                if (whosemove == 1)
                    player1.setX((int) playerpositionSlider.getValue());
                else player2.setX(widthwindow - player2.getWidth() - (int) playerpositionSlider.getValue());
            }
        });
        playerpositionSlider.setOnMouseClicked(arg0 ->
        {
            if (pathTransition.getStatus() != Animation.Status.RUNNING) {
                playerpositionTextField.setText(Double.toString((int) playerpositionSlider.getValue()));
                if (whosemove == 1)
                    player1.setX((int) playerpositionSlider.getValue());
                else player2.setX(widthwindow - player2.getWidth() - (int) playerpositionSlider.getValue());
            }
        });
        player1.setHeight(40);
        player1.setWidth(50);
        player1.setX(1);
        player1.setY(heightwindow-player1.getHeight());
        player1.setFill(Color.RED);

        player2.setHeight(40);
        player2.setWidth(50);
        player2.setX(500);
        player2.setY(heightwindow-player2.getHeight());
        player2.setFill(Color.BLUE);

        angleSlider.setTranslateY(10);
        angleSlider.setValue(angleSlider.getMin());
        angleTextField.setTranslateY(5);
        angleTextField.setTranslateX(150);
        angleTextField.setMaxWidth(30);
        angleTextField.setText("1.0");
        angleTextField.setEditable(false);
        angleLabel.setTranslateY(10);
        angleLabel.setTranslateX(190);

        speedSlider.setTranslateY(39);
        speedSlider.setValue(speedSlider.getMin());
        speedTextField.setTranslateY(33);
        speedTextField.setTranslateX(150);
        speedTextField.setMaxWidth(30);
        speedTextField.setText("1.0");
        speedTextField.setEditable(false);
        speedLabel.setTranslateY(37);
        speedLabel.setTranslateX(190);

        playerpositionSlider.setTranslateY(65);
        playerpositionSlider.setMax(widthwindow/2-player1.getWidth());
        playerpositionTextField.setTranslateY(60);
        playerpositionTextField.setTranslateX(150);
        playerpositionTextField.setMaxWidth(35);
        playerpositionTextField.setText("1.0");
        playerpositionTextField.setEditable(false);
        playerpositionLabel.setTranslateY(65);
        playerpositionLabel.setTranslateX(190);

        blockbullet.setWidth(5);
        blockbullet.setHeight(100);
        blockbullet.setX(widthwindow/2);
        blockbullet.setY(heightwindow-blockbullet.getHeight());

        whosemoveLabel.setTranslateX(widthwindow/2-60);
        whosemoveLabel.setTranslateY(heightwindow/4);
    }
    private void playbuttonmethod2(Stage primaryStage,Group root)
    {
        playbutton = arg0 -> {
            if (!animationisnow) {
                animationisnow = true;
                System.out.println("Button clicked");
                Circle bullet = new Circle();
                bullet.setRadius(5);
                bullet.setFill(Color.LIMEGREEN);
                bullet.setStroke(Color.BLACK);

                bullet.setTranslateX(whosemove == 1 ? player1.getX() : player2.getX() + player2.getWidth());
                bullet.setTranslateY(whosemove == 1 ? player1.getY() : player2.getY());
                bullet.setLayoutX(0);
                bullet.setLayoutY(0);

                Timeline timeline = new Timeline(new KeyFrame(Duration.millis(20),
                        new EventHandler<>() {
                            double dy = Math.cos(angleSlider.getValue() * Math.PI / 180);
                            double dx = Math.sin(angleSlider.getValue() * Math.PI / 180);
                            double a = dy;

                            @Override
                            public void handle(ActionEvent t) {
                                dy += 0.1;

                                if (whosemove == 1) {
                                    bullet.setTranslateX(bullet.getTranslateX() + Math.cos(-Math.asin(a)) * speedSlider.getValue());
                                    bullet.setTranslateY(bullet.getTranslateY() - Math.sin(-Math.asin(a)) * speedSlider.getValue() + dy);
                                } else {
                                    bullet.setTranslateX(bullet.getTranslateX() - Math.cos(-Math.asin(a)) * speedSlider.getValue());
                                    bullet.setTranslateY(bullet.getTranslateY() - Math.sin(-Math.asin(a)) * speedSlider.getValue() + dy);
                                }
                            }
                        }));
                timeline.setCycleCount(Timeline.INDEFINITE);
                timeline.play();
                if (timeline1 != null) timeline1.stop();
                timeline1 = new Timeline(new KeyFrame(Duration.millis(20),
                        t -> {
                            boolean playerhit = (whosemove == 1 ? bullet.getBoundsInParent().intersects(player2.getBoundsInParent()) : bullet.getBoundsInParent().intersects(player1.getBoundsInParent()));
                            boolean blockhit = (bullet.getBoundsInParent().intersects(blockbullet.getBoundsInParent()));
                            if (bullet.getTranslateY() > heightwindow || playerhit || blockhit) {
                                if (playerhit && !blockhit) {
                                    win = whosemove == 1 ? 1 : 2;
                                }
                                if (win != 0) {
                                    Label secondLabel = new Label(win == 1 ? "Красный победил!" : "Синий победил!");
                                    StackPane secondaryLayout = new StackPane();
                                    secondaryLayout.getChildren().add(secondLabel);
                                    Scene winscene = new Scene(secondaryLayout, 230, 100);
                                    Stage stagewin = new Stage();
                                    stagewin.setTitle("Победа!");
                                    stagewin.setScene(winscene);
                                    stagewin.setResizable(false);
                                    stagewin.centerOnScreen();
                                    stagewin.initOwner(primaryStage);
                                    stagewin.initModality(Modality.APPLICATION_MODAL);
                                    stagewin.show();
                                    stagewin.setOnCloseRequest(e -> {
                                        primaryStage.close();
                                        Stage st = new Stage();
                                        start(st);
                                    });
                                }
                                if (whosemove == 1) {
                                    whosemove++;
                                    playerpositionSlider.setValue(widthwindow - player2.getWidth() - player2.getX());
                                    angleBlue = angleSlider.getValue();
                                    angleSlider.setValue(angleRed);
                                    angleTextField.setText(Double.toString((int)angleRed-90));
                                    speedBlue = speedSlider.getValue();
                                    speedTextField.setText(Double.toString((int)speedRed));
                                    speedSlider.setValue(speedRed);
                                    whosemoveLabel.setText("Ход синего игрока!");
                                } else {
                                    whosemove--;
                                    playerpositionSlider.setValue(player1.getX());
                                    angleRed = angleSlider.getValue();
                                    angleSlider.setValue(angleBlue);
                                    angleTextField.setText(Double.toString((int)angleBlue-90));
                                    speedRed = speedSlider.getValue();
                                    speedTextField.setText(Double.toString((int)speedBlue));
                                    speedSlider.setValue(speedBlue);
                                    whosemoveLabel.setText("Ход красного игрока!");
                                }
                                if (win!=0) whosemoveLabel.setText("Ход красного игрока!");
                                playerpositionTextField.setText(Double.toString((int) playerpositionSlider.getValue()));
                                timeline.stop();
                                root.getChildren().remove(bullet);
                                bullet.setTranslateX(0);
                                bullet.setTranslateY(0);
                                animationisnow = false;
                            }
                        }));
                timeline1.setCycleCount(Timeline.INDEFINITE);
                timeline1.play();
                root.getChildren().addAll(bullet);
            }
        };
    }
    public static void main(String[] args) {
        launch(args);
    }

}  